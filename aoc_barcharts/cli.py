from datetime import datetime as dt
import json
import os
import re
import requests
import sys
import urllib.request

import pandas as pd
import pandas_alive

def user_scores(all_users, parts_completed, day, part):
    """Calculate the scores for all users, for a given part, on a given day."""
    # scores = dict.fromkeys(all_users, [])
    scores = {k: 0 for k in all_users}
    # Max score for each star is the number of users.
    score = len(all_users)
    for n, ts in sorted(parts_completed[(day, part)].items(), key = lambda item: item[1]):
        scores[n] += score
        score -= 1
    return scores

def main() -> None:
    year = os.environ['AOC_YEAR']
    leaderboard_id = os.environ['AOC_LEADERBOARD_ID']
    session_token = os.environ['AOC_SESSION_TOKEN']

    print("year: " + year)

    cookie = {'session': session_token}

    data = json.loads(requests.get('https://adventofcode.com/'+year+'/leaderboard/private/view/'+leaderboard_id+'.json', cookies=cookie).content)

    original_stdout = sys.stdout
    with open('data.csv', 'w') as f:
        sys.stdout = f

        print('time,name,stars,score')

        all_users = []
        for m in data['members'].values():
            user_name = m['name'] if m['name'] is not None else m['id']
            all_users.append(user_name)

        stars = {k: [] for k in all_users}
        parts_completed = {}

        all_parts = [(day, part) for day in range(1, 26) for part in range(1, 3)]

        for day, part in all_parts:
            parts_completed[(day, part)] = {}
            for m in data['members'].values():
                user_name = m['name'] if m['name'] is not None else m['id']
                try:
                    ts = dt.utcfromtimestamp(int(m['completion_day_level'][str(day)][str(part)]['get_star_ts']))
                    stars[user_name].append((ts, (day, part)))
                    parts_completed[(day, part)][user_name] = ts
                except: pass

        # Scores for each day and part, for all users.
        scores = {}
        for day, part in all_parts:
            scores[(day, part)] = user_scores(all_users, parts_completed, day, part)

        for user_name, timestamps in stars.items():
            timestamps.sort()
            stars = 1
            running_score = 0 # For the current user.
            for (ts, (day, part)) in timestamps:
                running_score += scores[(day, part)][user_name]
                print("{0},{1},{2},{3}".format(ts, user_name, stars, running_score))
                stars += 1

        sys.stdout = original_stdout

    df = pd.read_csv('data.csv')
    df['time'] = pd.to_datetime(df['time'])
    df.set_index('time')
    piv_df = df.pivot(values='score',index='time',columns='name').fillna(method='ffill')
    print(piv_df)
    piv_df.plot_animated(filename='aoc.gif', enable_progress_bar=True)

if __name__ == '__main__':
    main()
