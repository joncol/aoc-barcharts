{
  description = "Application packaged using poetry2nix";

  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixpkgs.url = "github:NixOS/nixpkgs";
  inputs.poetry2nix.url = "github:nix-community/poetry2nix";

  outputs = { self, nixpkgs, flake-utils, poetry2nix }:
    {
      # Nixpkgs overlay providing the application
      overlay = nixpkgs.lib.composeManyExtensions [
        poetry2nix.overlay
        (final: prev: {
          aoc-barcharts = prev.poetry2nix.mkPoetryApplication rec {
            projectDir = ./.;
            pyproject = ./pyproject.toml;
            poetrylock = ./poetry.lock;
          };
        })
      ];
    } // (flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ self.overlay ];
        };
      in rec {
        apps = { aoc-barcharts = pkgs.aoc-barcharts; };
        packages = pkgs.aoc-barcharts;
        defaultPackage = pkgs.aoc-barcharts;
        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [
            black
            ffmpeg
            gifsicle
            imagemagick
            poetry
            python39
          ];
        };
      }));
}
